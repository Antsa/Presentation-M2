\select@language {french}
\beamer@sectionintoc {2}{Le cadre de travail}{4}{0}{1}
\beamer@subsectionintoc {2}{1}{L'entreprise d'accueil: W3A}{4}{0}{1}
\beamer@subsectionintoc {2}{2}{Le projet AZOT}{6}{0}{1}
\beamer@sectionintoc {3}{Le sujet du stage: Extraction automatique d'évènements}{9}{0}{2}
\beamer@subsectionintoc {3}{1}{Contextes}{9}{0}{2}
\beamer@subsectionintoc {3}{2}{Objectif durant le stage}{12}{0}{2}
\beamer@sectionintoc {4}{La classification automatique de documents}{14}{0}{3}
\beamer@subsectionintoc {4}{1}{Définition}{14}{0}{3}
\beamer@subsectionintoc {4}{2}{Approche par le clustering}{16}{0}{3}
\beamer@subsectionintoc {4}{3}{Représentation de documents}{20}{0}{3}
\beamer@subsectionintoc {4}{4}{Application pour AZOT}{22}{0}{3}
\beamer@sectionintoc {5}{Observation des résultats obtenus}{24}{0}{4}
\beamer@sectionintoc {6}{A brief review of the MOOC followed}{32}{0}{5}
